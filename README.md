# Aurora Lamp

This is the source code for a desk lamp which lights up when there is a high probability
of seeing an aurora in Helsinki.

## How it works

The ESP32 has onboard wi-fi, allowing it to access the internet. Every five minutes, it
sends a request to the Finnish Meterological Institute to collect the latest
magnetometer readings from the Nurmijarvi station. The last 10 minutes of readings are
used to calculate the r-index. When the r-index is below the lowest threshold, the lamp
is off. If there is a small chance of seeing auroras, it begins to faintly glow green.
If there is a high chance, then it will glow green and blue. And if the r-index
surpasses the highest threshold, it glows bright purple.

## Installing

### Setting up the ESP32 chip for the first time

Follow the instructions [here](https://docs.micropython.org/en/latest/esp8266/tutorial/intro.html#intro) for installing esptool and downloading the latest firmware
for the specific model of your board. This only needs to be done once.

Do note that the USB address changes each time, so it won't necessarily be `/dev/tty.usbserial-2130` when you run it.

````bash
esptool.py --chip esp32 --port /dev/tty.usbserial-2130 erase_flash
esptool.py --chip esp32 --port /dev/tty.usbserial-2130 --baud 460800 write_flash -z 0x1000 ESP32_GENERIC-20240222-v1.22.2.bin
````

### Loading the program

1. Download [ampy](https://learn.adafruit.com/micropython-basics-load-files-and-run-code/install-ampy) if its not already installed.

2. Edit `main.py` and change the `SSID` and `PASSWORD` to your network's real credentials

````python
wlan.connect('SSID', 'PASSWORD')
````

3. Then transfer over `main.py` to the board.

````bash
ampy -p /dev/tty.usbserial-2130 put main.py /main.py
````

## Attribution

Magnetometer readings are provided by the IMAGE Magnetometer Array

We thank the institutes who maintain the IMAGE Magnetometer Array: Tromsø Geophysical Observatory of UiT the Arctic University of Norway (Norway), Finnish Meteorological Institute (Finland), Institute of Geophysics Polish Academy of Sciences (Poland), GFZ German Research Centre for Geosciences (Germany), Geological Survey of Sweden (Sweden), Swedish Institute of Space Physics (Sweden), Sodankylä Geophysical Observatory of the University of Oulu (Finland), DTU Technical University of Denmark (Denmark), and Science Institute of the University of Iceland (Iceland). The provisioning of data from AAL, GOT, HAS, NRA, VXJ, FKP, SIN, BOR, SCO and KUL is supported by the ESA contracts number 4000128139/19/D/CT as well as 4000138064/22/D/KS.
