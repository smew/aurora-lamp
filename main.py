import network
import urequests
import machine
import time

LED_RED = machine.Pin(4)
LED_GREEN = machine.Pin(5)
LED_BLUE = machine.Pin(2)

PWM_RED = machine.PWM(LED_RED)
PWM_GREEN = machine.PWM(LED_GREEN)
PWM_BLUE = machine.PWM(LED_BLUE)

AURORA_CHANCE_LOW = 70
AURORA_CHANCE_HIGH = 130
AURORA_CHANCE_EXTREME = 1000

def do_connect():
    """
    Connect to a local Wi-fi network
    """
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect('SSID', 'PASSWORD')
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())


def calculate_r_index(last_readings):
    """
    Calculate R-index based on latest magnetometer readings

    This calculates the rate of change of magnetometer readings in the X, Y, and Z axis
    over the past 10 minutes and returns the sum. Readings from over 5 minutes ago are
    given half weight. A more turbulent magnetic field will yield a higher R-index.

    https://space.fmi.fi/image/realtime/SSA/r-index/information/
    """
    x_sum = 0
    y_sum = 0
    z_sum = 0
    for i in range(1, 60):
        prev_row = last_readings[i-1].split()
        row = last_readings[i].split()
        x_diff = abs(float(row[6]) - float(prev_row[6]))
        y_diff = abs(float(row[7]) - float(prev_row[7]))
        z_diff = abs(float(row[8]) - float(prev_row[8]))
        if i <= 30:
            x_diff *= 0.5
            y_diff *= 0.5
            z_diff *= 0.5
        x_sum += x_diff
        y_sum += y_diff
        z_sum += z_diff
    total = int(x_sum + y_sum + z_sum)
    return total


def display(r_index):
    """
    Set the RGB LEDs based on latest r_index

    No chance or auroras: LEDS off
    Low chance: Green LED on
    High chance: Green and blue LEDs on
    Extreme chance: All Leds on
    """
    if r_index < AURORA_CHANCE_LOW:
        PWM_RED.duty(1023)
        PWM_GREEN.duty(1023)
        PWM_BLUE.duty(1023)
    elif r_index < AURORA_CHANCE_HIGH:
        PWM_RED.duty(1023)
        PWM_GREEN.duty(200)
        PWM_BLUE.duty(1023)
    elif r_index < AURORA_CHANCE_EXTREME:
        PWM_RED.duty(900)
        PWM_GREEN.duty(0)
        PWM_BLUE.duty(600)
    else:
        PWM_RED.duty(500)
        PWM_GREEN.duty(0)
        PWM_BLUE.duty(0)


def get_readings():
    """
    Get the latest magnetometer readings from the Nurmijarvi research station
    """
    url = "https://space.fmi.fi/image/realtime/UT/NUR/NURdata_01.txt"
    response = urequests.get(url)
    last_readings = response.text.split('\n')[-61:-1]
    response.close()
    return last_readings


def main():
    PWM_RED.freq(500)
    PWM_GREEN.freq(500)
    PWM_BLUE.freq(500)

    PWM_RED.duty(1023)
    PWM_GREEN.duty(1023)
    PWM_BLUE.duty(1023)

    while True:
        do_connect()
        last_readings = get_readings()
        r_index = calculate_r_index(last_readings)
        display(r_index)
        time.sleep(300)

main()
